﻿using API.Service;
using infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;

namespace API.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        UserService userService = new UserService();
        
        [Route("get-users")]
        [HttpGet]
        public object GetUsers()
        {
            var result = userService.GetAllUsers();
            return JsonSerializer.Serialize(result);
        }
        
        [Route("update-user")]
        [HttpPost]
        public void UpdateUser(User user)
        {
            userService.Update(user);
        }

        [Route("create-user")]
        [HttpPost]
        public void CreateUser(User user)
        {
            userService.Create(user);
        }

        [Route("delete-user")]
        [HttpPost]
        public void DeleteUser(User user)
        {
            userService.Delete(user);
        }
    }
}
