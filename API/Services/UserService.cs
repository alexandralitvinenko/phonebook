﻿using infrastructure;
using infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Service
{
    public class UserService
    {
        IUsersRepository db;

        public UserService()
        {
            db = new UsersRepository();
        }

        public List<User> GetAllUsers()
        {
            return db.GetUsersList().ToList<User>();
        }

        public void Create(User user)
        {
            user.Id = Guid.NewGuid();
            db.Create(user);
        }

        public void Update(User user)
        {
            db.Update(user);
        }

        public void Delete(User user)
        {
            db.Delete(user);
        }
    }
}
