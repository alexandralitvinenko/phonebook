## PhoneBook Application

To run this application please do the following:
1. Load solution, install/update NuGet Packages if need and apply DB migrations from infrastructure project (for example by using Package Manager Console in Visual Studio)
2. Run API project in Debug
3. Run vue-client project by commands `npm i` and `npm run serve`
