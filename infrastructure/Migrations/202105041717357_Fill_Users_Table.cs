﻿namespace infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fill_Users_Table : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO [dbo].[Users]
           ([Name]
           ,[LastName]
           ,[Email])
     VALUES
           ('Tom',
           'Black',
           'tom@example.com')
     INSERT INTO [dbo].[Users]
           ([Name]
           ,[LastName]
           ,[Email])
     VALUES
           ('Kate',
           'Test',
           'kate@example.com')
    INSERT INTO [dbo].[Users]
           ([Name]
           ,[LastName]
           ,[Email])
     VALUES
           ('Leo',
           'First',
           'leo@example.com')
    INSERT INTO [dbo].[Users]
           ([Name]
           ,[LastName]
           ,[Email])
     VALUES
           ('Kate',
           'Test',
           'kate@example.com')
    INSERT INTO [dbo].[Users]
           ([Name]
           ,[LastName]
           ,[Email])
     VALUES
           ('Tom',
           'Test',
           'tomtest@example.com')
INSERT INTO [dbo].[Users]
           ([Name]
           ,[LastName]
           ,[Email])
     VALUES
           ('Tom',
           'Second',
           'tom2@example.com')
");
        }
        
        public override void Down()
        {
            Sql(@"TRUNCATE TABLE [dbo].[Users]");
        }
    }
}
