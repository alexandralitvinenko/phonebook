﻿using System;
using System.Collections.Generic;

namespace infrastructure.Repositories
{
    public interface IUsersRepository : IDisposable
    {
        IEnumerable<User> GetUsersList();
        void Create(User user);
        void Update(User user);
        void Delete(User user);
    }
}
