﻿using System;
using System.Collections.Generic;

namespace infrastructure.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private bool disposed = false;
        private Context db;

        public UsersRepository()
        {
            this.db = new Context();
        }

        public void Create(User user)
        {
            if (user != null)
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
        }

        public void Update(User user)
        {
            var currentUser = db.Users.Find(user.Id);
            if (currentUser != null)
            {
                currentUser.Name = user.Name;
                currentUser.LastName = user.LastName;
                currentUser.Email = user.Email;
                currentUser.BirthDay = user.BirthDay;
                db.SaveChanges();
            }
        }

        public void Delete(User user)
        {
            var currentUser = db.Users.Find(user.Id);
            if (currentUser != null)
            {
                db.Users.Remove(currentUser);
                db.SaveChanges();
            }
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<User> GetUsersList()
        {
            return db.Users;
        }
    }
}
